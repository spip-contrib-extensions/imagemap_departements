<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function agrandir($coords, $fact = 1){
	$Tcoords = explode(',', $coords);
	$Tret = [];
	foreach ($Tcoords as $c) {
		$c = intval($c);
		$Tret[] = ($c * $fact);
	} 
	return join(',', $Tret);
}

