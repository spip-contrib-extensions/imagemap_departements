<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin imapdepart
 *
 * @plugin     imapdepart
 * @copyright  2010-2022
 * @author     
 * @licence    GNU/GPL
 * @package    SPIP\imapdepart\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin imapdepart
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function imapdepart_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();
	$maj['create'] = array(
		array('maj_tables', array('spip_imap_departements')),
		array('imapdepart_importer_donnees')
	);
	
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Importer les données dans la base
 *
   Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function imapdepart_importer_donnees() {
	spip_log('import go','imapo');

	// si elle est vide, remplir la table a partir du fichier CSV des departements
	$nb_departs = sql_countsel("spip_imap_departements");
	if ($nb_departs == 0) {
		$chem_csv = find_in_path('imap_departements.csv');
		$Tdepart = file($chem_csv);
		$Ta_inserer = array();
		foreach ($Tdepart as $d){
			$Td = explode(";",$d);
			$Ta_inserer[] = [
				"id_departement" => "NULL",
				"num_departement" => $Td[0],
				"nom" => $Td[1],
				"region" => $Td[2],
				"nom_web" => $Td[3],
				"coordonnees" => $Td[4]
			];
		}
		sql_insertq_multi("spip_imap_departements", $Ta_inserer);
	}
}


/**
 * Fonction de désinstallation du plugin imapdepart
 * 
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function imapdepart_vider_tables($nom_meta_base_version) {
	sql_drop_table("spip_imap_departements");

	effacer_meta($nom_meta_base_version);
}

